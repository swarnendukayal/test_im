FROM alpine:3.4

RUN apk update
RUN apk upgrade
RUN apk add openjdk8-jre-base bash curl zip wget

RUN wget https://releases.jfrog.io/artifactory/artifactory-pro/org/artifactory/pro/jfrog-artifactory-pro/6.23.38/jfrog-artifactory-pro-6.23.38.zip && unzip jfrog-artifactory-pro-6.23.38.zip

RUN rm jfrog-artifactory-pro-6.23.38.zip

EXPOSE 8081

CMD ./artifactory-pro-6.23.38/bin/artifactory.sh

